package main

import (
        "github.com/hashicorp/terraform/helper/schema"
        api "github.com/vmware/go-vmware-hcx"
        "log"
        //"io/ioutil"
        "time"

)

func resourcel2extension() *schema.Resource {
        return &schema.Resource{
                Create: resourcel2extensionCreate,
                Read:   resourcel2extensionRead,
                Update: resourcel2extensionUpdate,
                Delete: resourcel2extensionDelete,

                Schema: map[string]*schema.Schema{
                        "gateway": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "netmask": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "destination_endpoint_id": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "destination_resource_id": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "source_network_network_id": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "source_network_network_type": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "destination_network_gateway_id": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "source_appliance_appliance_id": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "proximity_routing": &schema.Schema{
                                Type:     schema.TypeBool,
                                Optional: true,
                                Default: false,
                        },
                        "egress_optimization": &schema.Schema{
                                Type:     schema.TypeBool,
                                Optional: true,
                                Default: false,
                        },
                        "strech_id": &schema.Schema{
                                Type:     schema.TypeString,
                                Computed: true,
                        },
                        "status": &schema.Schema{
                                Type:     schema.TypeString,
                                Computed: true,
                        },
                },
        }
}

func resourcel2extensionCreate(d *schema.ResourceData, m interface{}) error {
        client := m.(*api.APIClient)
        
        body := api.L2ExtensionsPost{
                Gateway: d.Get("gateway").(string),
                Netmask: d.Get("netmask").(string),
                Destination: api.HybridityApiL2ExtensionsDestination{
                        EndpointId: d.Get("destination_endpoint_id").(string),
                        ResourceId: d.Get("destination_resource_id").(string),
                },
                SourceNetwork: api.HybridityApiL2ExtensionsSourceNetwork{
                        NetworkId: d.Get("source_network_network_id").(string),
                        NetworkType: d.Get("source_network_network_type").(string),
                },
                DestinationNetwork: api.HybridityApiL2ExtensionsDestinationNetwork{
                        GatewayId: d.Get("destination_network_gateway_id").(string),
                },
                SourceAppliance: api.HybridityApiL2ExtensionsSourceAppliance{
                        ApplianceId: d.Get("source_appliance_appliance_id").(string),
                },
                Features: api.InlineResponse200Features{
                        ProximityRouting: d.Get("proximity_routing").(bool),
                        EgressOptimization: d.Get("egress_optimization").(bool),
                },
        }
        
        log.Println(body)
        result, _, err := client.L2ExtensionsApi.CreateNetworkExtension(client.Context, body)
        log.Println(result)
        log.Println(err)

        d.SetId(result.Id)
        d.Set("status", "Extending")

        for ok := true; ok; ok = (d.Get("status").(string) == "Extending") {
                getNetworkExtensions(d, m)
                time.Sleep(5 * time.Second)
        }

        return resourcel2extensionRead(d, m)
}

func getNetworkExtensions(d *schema.ResourceData, m interface{}) bool {
        client := m.(*api.APIClient)
        found := false

        log.Println("#########################################")
                result, _, err := client.L2ExtensionsApi.ListNetworkExtensions(client.Context)

                if (err == nil) {
                                for _, item := range result.Items {                                  
                                        if (item.SourceNetwork.NetworkId == d.Get("source_network_network_id")) {
                                                // We found the good one
                                                found = true
                                                log.Println("found!")
                                                d.Set("strech_id", item.StretchId)
                                                d.Set("status", item.OperationStatus.State)
                                        }                                    
                                }
                        }
        return found
}


func resourcel2extensionRead(d *schema.ResourceData, m interface{}) error {
        getNetworkExtensions(d, m)
    
        return nil
}

func resourcel2extensionUpdate(d *schema.ResourceData, m interface{}) error {

        return resourcel2extensionRead(d, m)
}

func resourcel2extensionDelete(d *schema.ResourceData, m interface{}) error {
        client := m.(*api.APIClient)

        //options := api.DeleteNetworkExtensionOpts{}
        d.Set("status", "Tearing down")
        
        result, _, err := client.L2ExtensionsApi.DeleteNetworkExtension(client.Context, d.Get("strech_id").(string), nil)
        log.Println(result)
        log.Println(err)

        for ok := true; ok; ok = getNetworkExtensions(d, m) {
                time.Sleep(5 * time.Second)
        }

        return nil
}