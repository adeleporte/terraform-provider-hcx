package main

import (
	"context"

	"log"


	"github.com/hashicorp/terraform/helper/schema"
	api "github.com/vmware/go-vmware-hcx"
)

// Provider ...
func Provider() *schema.Provider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"host": &schema.Schema{
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("HCX_HOST", nil),
			},
			"username": &schema.Schema{
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("HCX_USERNAME", nil),
			},
			"password": &schema.Schema{
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("HCX_PASSWORD", nil),
			},
		},
		ResourcesMap: map[string]*schema.Resource{
			"hcx_l2extension": resourcel2extension(),
			"hcx_replication": resourcereplication(),
		},
		ConfigureFunc: providerConfigure,
	}
}

func providerConfigure(d *schema.ResourceData) (interface{}, error) {
	host := d.Get("host").(string)
	username := d.Get("username").(string)
	password := d.Get("password").(string)

	cfg := api.Configuration{
		BasePath:  "https://" + host,
		Host:      host,
		Scheme:    "https",
		UserAgent: "terraform-provider-vrni/1.0",
	}

	client := api.NewAPIClient(&cfg)

	resp, _ := client.SessionsApi.HybridityApiSessionsPost(context.Background(),
		"application/json",
		"application/json",
		api.InlineObject{
			Username: username,
			Password: password,
		})

	log.Println("************************************************************")
	//log.Println(resp)
	//log.Println(err)

	auth := context.WithValue(context.Background(), api.ContextAccessToken, resp.Header.Get("x-hm-authorization"))
	client.Context = auth
	log.Println(auth)

	

	return client, nil
}
