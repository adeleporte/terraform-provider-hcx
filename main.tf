provider "hcx" {
    host                                    = "192.168.7.50"
    username                                = "administrator@vsphere.local"
    password                                = "VMware1!"
}
/*
resource "hcx_l2extension" "my-extension" {
    gateway                                 = "1.1.1.1"
    netmask                                 = "255.255.255.0"
    destination_endpoint_id                 = "20190522184053630-9acbfb17-daf8-4b48-9cec-6b499d8e8fc0"
    destination_resource_id                 = "56d58607-fd5c-4206-84cb-acb4ed88276f"
    source_network_network_id               = "/infra/segments/hcx-test"
    source_network_network_type             = "NsxtSegment"
    destination_network_gateway_id          = "cgw"
    source_appliance_appliance_id           = "2b4d02ff-e766-47fa-9d65-7c40e8d332eb"
    proximity_routing                       = false
    egress_optimization                     = false
}
*/
resource "hcx_replication" "my-replication" {
    destination_endpoint_type               = "VC"
    destination_endpoint_id                 = "20190522184053630-9acbfb17-daf8-4b48-9cec-6b499d8e8fc0"
    destination_endpoint_name               = "HCX Cloud - vmc-set-eu-central-17"
    destination_resource_type               = "VC"
    destination_resource_id                 = "56d58607-fd5c-4206-84cb-acb4ed88276f"
    destination_resource_name               = "vcenter.sddc-18-195-38-9.vmwarevmc.com"

    // Source
    source_endpoint_type                    = "VC"
    source_endpoint_id                      = "20190828224504110-93327455-4d68-4b2c-87e6-5395860dfb7c"
    source_endpoint_name                    = "hcx-mgr-enterprise"
    source_resource_type                    = "VC"
    source_resource_id                      = "1700e87b-168b-4ecb-afaa-cd165f73ce9f"
    source_resource_name                    = "vcsa.branch.vcn.lab"

    // VM details
    vm_entity_id                            = "vm-202"
    vm_entity_name                          = "VCNAppUS-web"

    // Replication params
    replication_params_rpo                  = 15
    replication_params_snapshot_interval    = 1
    replication_params_snapshot_number      = 1
    replication_params_quiesce              = false
    replication_params_compression          = false

    // Replication Info
    replication_type                        = "dr"

    // Decision Rules
    decision_remove_snapshots               = false
    decision_remove_isos                    = false

    // Seed Info
    seed_use_seed                           = false

    // Storage
    storage_disk_provision_type             = "datastore"
    storage_datastore_id                    = "datastore-45"
    storage_datastore_name                  = "WorkloadDatastore"
    storage_profile_id                      = "datastore-45"

    // Placement
    placement_0_container_type              = "resourcePool"
    placement_0_container_id                = "resgroup-44"
    placement_0_container_name              = "resgroup-44"
    placement_1_container_type              = "dataCenter"
    placement_1_container_id                = "datacenter-3"
    placement_1_container_name              = "datacenter-3"
    placement_2_container_type              = "folder"
    placement_2_container_id                = "group-v141"
    placement_2_container_name              = "group-v141"

    // Networks
    network_src_network_name                = "MyApp-L2-LS"
    network_src_network_type                = "OpaqueNetwork"
    network_src_network_value               = "network-o201"
    network_src_network_href                = "network-o201"
    network_dest_network_name               = "L2E_hcx-test-71699-1700e87b"
    network_dest_network_type               = "NsxtSegment"
    network_dest_network_value              = "/infra/tier-1s/cgw/segments/L2E_hcx-test-71699-1700e87b"
    network_dest_network_href               = "/infra/tier-1s/cgw/segments/L2E_hcx-test-71699-1700e87b"
}