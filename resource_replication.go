package main

import (
        "github.com/hashicorp/terraform/helper/schema"
        api "github.com/vmware/go-vmware-hcx"
        "log"
        //"io/ioutil"
        "time"

)

func resourcereplication() *schema.Resource {
        return &schema.Resource{
                Create: resourcereplicationCreate,
                Read:   resourcereplicationRead,
                Update: resourcereplicationUpdate,
                Delete: resourcereplicationDelete,

                Schema: map[string]*schema.Schema{
                        // Destination
                        "destination_endpoint_type": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "destination_endpoint_id": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "destination_endpoint_name": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "destination_resource_type": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "destination_resource_id": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "destination_resource_name": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        // Source
                        "source_endpoint_type": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "source_endpoint_id": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "source_endpoint_name": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "source_resource_type": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "source_resource_id": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "source_resource_name": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        // VM details
                        "vm_entity_id": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "vm_entity_name": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        // Replication params
                        "replication_params_rpo": &schema.Schema{
                                Type:     schema.TypeInt,
                                Required: true,
                        },
                        "replication_params_snapshot_interval": &schema.Schema{
                                Type:     schema.TypeInt,
                                Required: true,
                        },
                        "replication_params_snapshot_number": &schema.Schema{
                                Type:     schema.TypeInt,
                                Required: true,
                        },
                        "replication_params_quiesce": &schema.Schema{
                                Type:     schema.TypeBool,
                                Required: true,
                        },
                        "replication_params_compression": &schema.Schema{
                                Type:     schema.TypeBool,
                                Required: true,
                        },
                        // Replication Info
                        "replication_type": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        // Decision Rules
                        "decision_remove_snapshots": &schema.Schema{
                                Type:     schema.TypeBool,
                                Required: true,
                        },
                        "decision_remove_isos": &schema.Schema{
                                Type:     schema.TypeBool,
                                Required: true,
                        },
                        // Seed Info
                        "seed_use_seed": &schema.Schema{
                                Type:     schema.TypeBool,
                                Required: true,
                        },
                        // Storage
                        "storage_disk_provision_type": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "storage_datastore_id": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "storage_datastore_name": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "storage_profile_id": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        // Placement
                        "placement_0_container_type": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "placement_0_container_id": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "placement_0_container_name": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "placement_1_container_type": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "placement_1_container_id": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "placement_1_container_name": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "placement_2_container_type": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "placement_2_container_id": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "placement_2_container_name": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        // Networks
                        "network_src_network_name": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "network_src_network_type": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "network_src_network_value": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "network_src_network_href": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "network_dest_network_name": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "network_dest_network_type": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "network_dest_network_value": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },
                        "network_dest_network_href": &schema.Schema{
                                Type:     schema.TypeString,
                                Required: true,
                        },

                },
        }
}

func resourcereplicationCreate(d *schema.ResourceData, m interface{}) error {
        client := m.(*api.APIClient)
        
        request := []api.Nls10VsphereReplicationProtectRecoverRequest{{
                Destination: api.Nls10VsphereReplicationQueryResponseSource{
                        EndpointType: d.Get("destination_endpoint_type").(string),
                        EndpointId: d.Get("destination_endpoint_id").(string),
                        EndpointName: d.Get("destination_endpoint_name").(string),
                        ResourceType: d.Get("destination_resource_type").(string),
                        ResourceId: d.Get("destination_resource_id").(string),
                        ResourceName: d.Get("destination_resource_name").(string),
                },
                Source: api.Nls10VsphereReplicationQueryResponseSource{
                        EndpointType: d.Get("source_endpoint_type").(string),
                        EndpointId: d.Get("source_endpoint_id").(string),
                        EndpointName: d.Get("source_endpoint_name").(string),
                        ResourceType: d.Get("source_resource_type").(string),
                        ResourceId: d.Get("source_resource_id").(string),
                        ResourceName: d.Get("source_resource_name").(string),
                },
                Networks: api.Nls10VsphereReplicationQueryResponseNetworks{
                        RetainMac: false,
                        TargetNetworks: 
                                []api.Nls10VsphereReplicationQueryResponseNetworksTargetNetworks{{
                                        SrcNetworkName: d.Get("network_src_network_name").(string),
                                        SrcNetworkType: d.Get("network_src_network_type").(string),
                                        SrcNetworkValue: d.Get("network_src_network_value").(string),
                                        SrcNetworkHref: d.Get("network_src_network_href").(string),
                                        DestNetworkName: d.Get("network_dest_network_name").(string),
                                        DestNetworkType: d.Get("network_dest_network_type").(string),
                                        DestNetworkValue: d.Get("network_dest_network_value").(string),
                                        DestNetworkHref: d.Get("network_dest_network_href").(string),               
                                }},

                },
                Placement: []api.Nls10VsphereReplicationQueryRequestPlacement{
                                {
                                        ContainerType: d.Get("placement_0_container_type").(string),
                                        ContainerName: d.Get("placement_0_container_name").(string),
                                        ContainerID: d.Get("placement_0_container_id").(string),
                                },
                                {
                                        ContainerType: d.Get("placement_1_container_type").(string),
                                        ContainerName: d.Get("placement_1_container_name").(string),
                                        ContainerID: d.Get("placement_1_container_id").(string),
                                },
                                {
                                        ContainerType: d.Get("placement_2_container_type").(string),
                                        ContainerName: d.Get("placement_2_container_name").(string),
                                        ContainerID: d.Get("placement_2_container_id").(string),
                                }},
                ReplicationInfo: api.Nls10VsphereReplicationQueryRequestReplicationInfo{
                        Type: d.Get("replication_type").(string),
                },
                ReplicationParams: api.Nls10VsphereReplicationQueryResponseReplicationParams{
                        Rpo: d.Get("replication_params_rpo").(int),
                        SnapshotInterval: d.Get("replication_params_snapshot_interval").(int),
                        NoOfSnapshots: d.Get("replication_params_snapshot_number").(int),
                        QuiesceGuestEnabled: d.Get("replication_params_quiesce").(bool),
                        NetworkCompressionEnabled: d.Get("replication_params_compression").(bool),                  
                },
                SeedInfo: api.Nls10VsphereReplicationQueryResponseSeedInfo{
                        UseSeed: d.Get("seed_use_seed").(bool),
                },
                Storage: api.Nls10VsphereReplicationQueryRequestStorage{
                        DatastoreName: d.Get("storage_datastore_name").(string),
                        DatastoreID: d.Get("storage_datastore_id").(string),
                        ID: d.Get("storage_profile_id").(string),
                        Type: d.Get("storage_disk_provision_type").(string),
                },
                VMDetails: api.Nls10VsphereReplicationQueryRequestVmDetails{
                        EntityID: d.Get("vm_entity_id").(string),
                        EntityName: d.Get("vm_entity_name").(string),
                },
        }}
        log.Println(request)
        result, resp, err := client.ReplicationsApi.HybridityApiReplicationsactionprotectPost(client.Context, "protect", request)
        log.Print(result, resp, err)

        
        for ok := true; ok; ok = !getReplication(d, m) {
                time.Sleep(5 * time.Second)
        }


        return resourcereplicationRead(d, m)
}


func getReplication(d *schema.ResourceData, m interface{}) bool {
        client := m.(*api.APIClient)
        request := api.Nls10VsphereReplicationQueryRequest{
                Type: "report",
        }

        found := false

        log.Println("#########################################")
                result, _, err := client.ReplicationsApi.HybridityApiReplicationsactionqueryPost(client.Context, "query", request)
                log.Println(err)
                log.Println(result)
                if (err == nil) {
                                for _, item := range result.Replications {  
                                        log.Println(item.SourceEntity)                                
                                        if (item.SourceEntity.EntityId == d.Get("vm_entity_id")) {
                                                // We found the good one
                                                found = true
                                                log.Println("found!")
                                                d.SetId(item.ReplicationId)
                                                //d.Set("status", item.OperationStatus.State)
                                        }                                    
                                }
                        }
        return found
}

func resourcereplicationRead(d *schema.ResourceData, m interface{}) error {
        getReplication(d, m)

        return nil
}

func resourcereplicationUpdate(d *schema.ResourceData, m interface{}) error {

        return resourcereplicationRead(d, m)
}

func resourcereplicationDelete(d *schema.ResourceData, m interface{}) error {
        client := m.(*api.APIClient)

        request := []api.Nls10VsphereReplicationRecoverRequest{{
                Destination: api.Nls10VsphereReplicationQueryResponseSource{
                        EndpointType: d.Get("destination_endpoint_type").(string),
                        EndpointId: d.Get("destination_endpoint_id").(string),
                        EndpointName: d.Get("destination_endpoint_name").(string),
                        ResourceType: d.Get("destination_resource_type").(string),
                        ResourceId: d.Get("destination_resource_id").(string),
                        ResourceName: d.Get("destination_resource_name").(string),
                },
                Source: api.Nls10VsphereReplicationQueryResponseSource{
                        EndpointType: d.Get("source_endpoint_type").(string),
                        EndpointId: d.Get("source_endpoint_id").(string),
                        EndpointName: d.Get("source_endpoint_name").(string),
                        ResourceType: d.Get("source_resource_type").(string),
                        ResourceId: d.Get("source_resource_id").(string),
                        ResourceName: d.Get("source_resource_name").(string),
                },
                ResourceID: d.Get("destination_resource_id").(string),
                ReplicationID: d.Id(),
        }}

        result, resp, err := client.ReplicationsApi.HybridityApiReplicationsactionremoveProtectionPost(client.Context, "removeProtection", request)
        log.Print(result, resp, err)

        return nil
}